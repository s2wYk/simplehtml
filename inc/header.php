<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="css/home.css">
	<title>Bootstrap 4</title>
</head>
<body>
	<div id="wrapper">
		<header id="header" class="header">
			<div class="container">
				<nav class="navbar navbar-expand-md navbar-light bg-light">
					<a class="navbar-brand" href="#">Navbar</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto nav" id="nav">
							<li class="nav-item active">
								<a class="nav-link" href="index.html">Home</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="product.html">PRODUCTS</a>
								<ul>
									<li><a href="#"> Club Services</a></li>
									<li><a href="#">Membership</a></li>
									<li><a href="#">Community</a></li>
								</ul>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="services.html">SERVICE</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="distribution.html">GLOBAL DISTRIBUTION</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="contact.html">CONTACT US </a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="about.html">ABOUT US</a>
							</li>
						</ul>
					</div>
					<form action="#" class="search-form" method="post">
						<a href="javascript:void(0)" class="search-opner"><i class="fa fa-search"></i></a>
						<div class="field">
							<input placeholder="search" class="input" type="search">
							<button type="submit"><i class="fa fa-search"></i></button>
						</div>
					</form>
				</nav>
			</div>
		</header>